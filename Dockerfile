# Установка базового образа Ubuntu
FROM ubuntu:22.04
WORKDIR /home/vagrant/es
RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y tzdata && \
    ln -fs /usr/share/zoneinfo/Asia/Almaty /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean
# Обновление и установка необходимых пакетов
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    software-properties-common \
    curl \
    gnupg2 \
    build-essential \
    && \
    rm -rf /var/lib/apt/lists/*

# Установка PHP 8.1 и необходимых расширений
RUN add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    php8.1 \
    php8.1-fpm \
    php8.1-cli \
    php8.1-mysql \
    php8.1-sqlite3 \
    php8.1-odbc \
    php8.1-xml \
    php8.1-curl \
    php8.1-mbstring \
    php8.1-zip \
    php8.1-bcmath \
    php8.1-gd \
    php8.1-intl \
    php-pear \
    php8.1-dev \
    && \
    rm -rf /var/lib/apt/lists/* && \
    update-alternatives --set php /usr/bin/php8.1

# Установка Nginx
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    nginx && \
    rm -rf /var/lib/apt/lists/*

# Установка Node.js и npm
RUN su -c 'curl -sL https://deb.nodesource.com/setup_18.x | bash -' && \
    apt-get install  -y --no-install-recommends \
    nodejs
#    rm -rf /var/lib/apt/lists/*

# Очистка кэша и временных файлов
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV DEBIAN_FRONTEND=noninteractive

# Копирование конфигурационных файлов
COPY nginx/default /etc/nginx/sites-available/default
COPY ssl /etc/ssl
COPY es .
COPY start.sh .
RUN chmod -R 777 /home/vagrant/es && chown -R www-data:www-data /home/vagrant/es
# Определение рабочей директории
# Открытие портов
EXPOSE 80
EXPOSE 443
RUN npm install -g npm@10.8.0 && npm install vite
RUN npm install -g pm2
#RUN pm2 start npm --name "my-app" -- run dev
# Запуск служб
#CMD service php8.1-fpm start && nginx && start.sh -g "daemon off;"
RUN chmod +x /home/vagrant/es/start.sh
ENTRYPOINT ["/home/vagrant/es/start.sh"]
