#!/bin/bash


# Start PHP-FPM
service php8.1-fpm start

# Start Nginx
#nginx -g "daemon off;"

# Check if Node.js is installed
if ! command -v node &> /dev/null; then
    echo "Node.js is not installed. Please install Node.js and pm2."
    exit 1
fi

# Check if pm2 is installed
if ! command -v pm2 &> /dev/null; then
    echo "pm2 is not installed. Please install pm2."
    exit 1
fi

# Start pm2 with your application
pm2 start npm --name "my-app" -- run dev
nginx -g "daemon off;"
