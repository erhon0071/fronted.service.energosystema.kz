import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/order',
      name: 'OrderIndex',
      component: () => import('../views/order/OrderIndex.vue'),
    },
    {
      path: '/order/create',
      name: 'OrderCreate',
      component: () => import('../views/order/OrderCreate.vue')
    },
    {
      path: '/order/:id/edit',
      name: 'OrderEdit',
      component: () => import('../views/order/OrderEdit.vue')
    },
  ]
})

export default router
