import {ref, reactive} from "vue";
import axios from "axios";
import {useRouter} from "vue-router";
import { v4 as uuidv4 } from "uuid";

axios.defaults.baseURL = "https://192.168.0.74:8443/api/v1/";
export default function useOrders(){

     const uuid = uuidv4();
    const currentDate = new Date().toLocaleDateString();
    const orders = ref([]);
    const staff = ref([]);
    const order = ref([]);
    const errors = ref({});
    const getEmployeeErrors = ref({});
    const services = ref([]);
    const selectedServices = ref([]);
    const router = useRouter();
    

    const getOrders = async () => {
        const response = await axios.get('order');
        orders.value = response.data.data;
        console.log(response.data.data);
    };
    

    const getOrder = async (id) => {
        const response = await axios.get("order" + id);
        order.value = response.data.data;
    };


    const storeOrder = async (data, selectedServices) =>{
        try {
            await axios.post("order", data);
            await axios.post("services", selectedServices);
            await router.push({name: "OrderIndex"});
        } catch (error){
            if(error.response.status === 422 ){
                errors.value = error.response.data.errors;
            }
        }
    };

   
    const updateOrder = async(id) => {
        try{
            await axios.put("order/" + id, order.value);
            await router.push({name: "OrderIndex"});
        }catch (error){
            if(error.response.status === 422 ){
                errors.value = error.response.data.errors;
            }
        }
    };

    const getBilling = async (accountNumber) => {
        try {
         
          const response = await axios.get('billing/' + accountNumber );
          toast.value = false;
          
          return { 
            full_name: response.data.data.Subject_Name,
            address: response.data.data.Address_Name,
            meter_numbers: response.data.data.Serial_Numbers,
            phase_count: response.data.data.Phase_Count,
          };
        } catch (error) {
            if(error.response.status === 500){
            errors.value = ('Лицевой счет "'+ accountNumber + '"  не найден в базе биллинговой системы');
              toast.value = true;
               /*console.log(toast);*/
              return {
                full_name: '',
                address: '',
                meter_numbers: '',
                phase_count: '',                         
              }
            }
          console.error(errors.value);
          return null;
        }
      };

      const getPhase = async (meterNumber) => {
        try {
          const response = await axios.get('electric-meters/' + meterNumber );
          return {
            Phase_Count: response.data.data.Phase_Count,
          };
        } catch (error) {
          console.error("Ошибка при получении данных:", error);
          return null;
        }
      };

      /*const getServices = async (phaseCount) => {
        try {
          const response = await axios.get('services/' + phaseCount );
          services.value = response.data.data,
          return {
           
          };
        } catch (error) {
          console.error("Ошибка при получении данных:", error);
          return null;
        }
      };*/

      const getServices = async (phaseCount) => {
        const response = await axios.get('services/' + phaseCount );
        services.value = response.data.data;
        console.log(response.data.data);
    };

   /* const getServices = async (phaseCount) => {
     
        try {
          // const services = [];
          const response = await axios.get('services/' + phaseCount );
          
          for (const serviceData of response.data.data) {
            services.value.push({
              id: serviceData.id,
              tittle: serviceData.tittle,
              amount: serviceData.amount,
              uuid: uuid,
            });

          }
          return services;
          //const services = [];
        } catch (error) {
          console.error("Ошибка при получении данных счета:", error);
          return null;
        }
      };*/

    const storeServices = async (selectedServices) =>{
       try {
            //const json = JSON.stringify(selectedServices);
            await axios.post("services", selectedServices).then((response) => {
                console.log(response);
              });;
            await router.push({name: "OrderIndex"});
            
        } catch (error){
            if(error.response.status === 500 ){
                errors.value = "Ошибка при получении данных с Биллинговой системы";
            }
        }
    };

    const toast = ref(false);

    const getBillingEmployee = async () =>{
      try {
           
        const response = await axios.get('billing-employee');
        staff.value = response.data.data;
        console.log(response.data.data);
           
       } catch (error){
           if(error.response.status === 500 ){
               errors.value = 'Нет связи с базой Биллинговой системы!';
               toast.value = !toast.value;
               console.log(toast);
           }
       }
   };

  
    const destroyOrder = async(id) => {
        if(!window.confirm("Удалить запись?")){
            return;
        }
        await axios.delete("order/" + id);
        await  getOrder();
    }

    return {
        order,
        orders,
        staff,
        getOrders,
        getOrder,
        storeOrder,
        updateOrder,
        destroyOrder,
        getBilling,
        getPhase,
        getServices,
        getBillingEmployee,
        storeServices,
        services,
        errors,
        getEmployeeErrors,
        selectedServices,
        uuid,
        currentDate,
        toast,
    }
}
